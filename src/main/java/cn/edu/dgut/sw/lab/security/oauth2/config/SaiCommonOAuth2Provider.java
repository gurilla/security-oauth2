package cn.edu.dgut.sw.lab.security.oauth2.config;

import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.AuthenticationMethod;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;

/**
 * @author Sai
 * @see org.springframework.security.config.oauth2.client.CommonOAuth2Provider
 * @since 1.0
 * 2018-8-24
 */
@SuppressWarnings("unused")
public enum SaiCommonOAuth2Provider {

    WEIXIN {
        @Override
        public ClientRegistration.Builder getBuilder(String registrationId) {
            ClientRegistration.Builder builder = getBuilder(registrationId,
                    ClientAuthenticationMethod.POST, DEFAULT_REDIRECT_URL);
            builder.scope("snsapi_login");
            builder.authorizationUri("https://open.weixin.qq.com/connect/qrconnect");
            builder.tokenUri("https://api.weixin.qq.com/sns/oauth2/access_token");
            builder.userInfoUri("https://api.weixin.qq.com/sns/userinfo");
            builder.userNameAttributeName("openid");
            builder.userInfoAuthenticationMethod(AuthenticationMethod.FORM);
            builder.clientName("WeiXin");
            return builder;
        }
    },

    QQ {
        @Override
        public ClientRegistration.Builder getBuilder(String registrationId) {
            ClientRegistration.Builder builder = getBuilder(registrationId,
                    ClientAuthenticationMethod.POST, DEFAULT_REDIRECT_URL);
            builder.scope("get_user_info");
            builder.authorizationUri("https://graph.qq.com/oauth2.0/authorize");
            builder.tokenUri("https://graph.qq.com/oauth2.0/token");
            builder.userInfoUri("https://graph.qq.com/user/get_user_info");
            builder.userNameAttributeName("nickname");
            builder.userInfoAuthenticationMethod(AuthenticationMethod.FORM);
            builder.clientName("QQ");
            return builder;
        }
    },

    DGUT {
        @Override
        public ClientRegistration.Builder getBuilder(String registrationId) {
            ClientRegistration.Builder builder = getBuilder(registrationId,
                    ClientAuthenticationMethod.POST, DEFAULT_REDIRECT_URL);
            builder.scope("dgut");
            builder.authorizationUri("https://cas.dgut.edu.cn");
            builder.tokenUri("https://cas.dgut.edu.cn/ssoapi/v2/checkToken");
            builder.userInfoUri("https://cas.dgut.edu.cn/oauth/getUserInfo");
            builder.userNameAttributeName("username");
            builder.userInfoAuthenticationMethod(AuthenticationMethod.FORM);
            builder.clientName("Dgut");
            return builder;
        }
    },

    GITHUB {
        @Override
        public ClientRegistration.Builder getBuilder(String registrationId) {
            ClientRegistration.Builder builder = getBuilder(registrationId,
                    ClientAuthenticationMethod.BASIC, DEFAULT_REDIRECT_URL);
            builder.scope("read:user");
            builder.authorizationUri("https://github.com/login/oauth/authorize");
            builder.tokenUri("https://github.com/login/oauth/access_token");
            builder.userInfoUri("https://api.github.com/user");
            builder.userNameAttributeName("id");
            builder.clientName("GitHub");
            return builder;
        }
    };

    private static final String DEFAULT_REDIRECT_URL = "{baseUrl}/{action}/oauth2/code/{registrationId}";

    @SuppressWarnings("SameParameterValue")
    protected final ClientRegistration.Builder getBuilder(String registrationId,
                                                          ClientAuthenticationMethod method, String redirectUri) {
        ClientRegistration.Builder builder = ClientRegistration.withRegistrationId(registrationId);
        builder.clientAuthenticationMethod(method);
        builder.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE);
        builder.redirectUriTemplate(redirectUri);
        return builder;
    }

    /**
     * Create a new
     * {@link org.springframework.security.oauth2.client.registration.ClientRegistration.Builder
     * ClientRegistration.Builder} pre-configured with provider defaults.
     *
     * @param registrationId the registration-id used with the new builder
     * @return a builder instance
     */
    public abstract ClientRegistration.Builder getBuilder(String registrationId);
}
